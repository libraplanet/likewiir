#include <windows.h>
#include <SDL.h>
#include <wiiuse.h>

/**
 * SDL_Draw.h、SDL_Draw.cは
 * 『Bin溜り』様の『SDL描画関数』を使用させていただきました。
 * http://hp.vector.co.jp/authors/VA028002/sdl/index.html
 * SDLに線や円を描く関数がないとは思わなかった・・・。
 */
#include "SDL_Draw.h"

#define _USE_MATH_DEFINES
#include <math.h>

/**
 * 入力管理用共有体。
 * 32bit
 */
typedef union
{
	struct
	{
		int up : 1;
		int down : 1;
		int left : 1;
		int right : 1;
		int enter : 1;
		int num_return : 1;
		int escape : 1;
		int F11 : 1;
		int pad : 24;
	} key;
	unsigned int all;
}KEY_DATA;

/**
 * 入力データ管理構造体
 */
typedef struct
{
	KEY_DATA trg, rpt, rol;
}INPUT_DATA;

/**
 * 入力管理作業構造体
 */
typedef struct
{
	INPUT_DATA data, buf;
	SDLKey rol_key;
	int rol_cnt;
}INPUT_WORK;

#define RAD_LOG_MAX (10)
typedef struct
{
	wiimote** wiimotes;
	int maintask, subtask;
	double log_rad[RAD_LOG_MAX];
}REMO_WORK;

/**
 *
 */
typedef struct
{
	INPUT_WORK input;
	REMO_WORK remo;
	SDL_Surface *screen;
	SDL_Surface *img_off;
	int fps_anim;
	float rad;
}WORK;

#define DEG2RAD(deg) ((deg) * M_PI / 180)
#define RAD2DEG(rad) ((rad) * 180 / M_PI)

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
#define RMASK 0xff000000
#define GMASK 0x00ff0000
#define BMASK 0x0000ff00
#define AMASK 0x000000ff
#else
#define RMASK 0x000000ff
#define GMASK 0x0000ff00
#define BMASK 0x00ff0000
#define AMASK 0xff000000
#endif



/**
 *
 */
void trace(const char *str)
{
#ifdef _DEBUG
	OutputDebugString(str);
	OutputDebugString("\n");
#endif
}

/**
 *
 */
void initInputWork(INPUT_WORK *work)
{
	work->buf.trg.all = 0;
	work->buf.rpt.all = 0;
	work->buf.rol.all = 0;
	work->data.trg.all = 0;
	work->data.rpt.all = 0;
	work->data.rol.all = 0;
	work->rol_cnt = SDLK_UNKNOWN;
	work->rol_key = -1;
}

KEY_DATA getAddedBitData(SDLKey type)
{
	KEY_DATA ret;
	ret.all = 0;

	if(type == SDLK_UP)
	{
		ret.key.up = 1;
	}
	else if(type == SDLK_DOWN)
	{
		ret.key.down = 1;
	}
	else if(type == SDLK_LEFT)
	{
		ret.key.left = 1;
	}
	else if(type == SDLK_RIGHT)
	{
		ret.key.right = 1;
	}
	else if(type == SDLK_RETURN)
	{
		ret.key.enter = 1;
	}
	else if(type == SDLK_KP_ENTER)
	{
		ret.key.num_return;
	}
	else if(type == SDLK_ESCAPE)
	{
		ret.key.escape = 1;
	}
	else if(type == SDLK_F11)
	{
		ret.key.F11 = 1;
	}

	return ret;
}

/**
 *
 */
void pressKey(INPUT_WORK *work, SDLKey type)
{
	KEY_DATA bit = getAddedBitData(type);
	work->buf.rpt.all |= bit.all;
	work->buf.trg.all |= bit.all;
	if(bit.all != 0)
	{
		work->rol_key = type;
		work->rol_cnt = (60 / 2);
	}
}

/**
 *
 */
void releaseKey(INPUT_WORK *work, SDLKey type)
{
	KEY_DATA bit = getAddedBitData(type);
	work->buf.rpt.all &= ~bit.all;
	if(work->rol_key == type)
	{
		work->rol_key = SDLK_UNKNOWN;
		work->rol_cnt = -1;
	}
}


/**
 *
 */
void updateInput(INPUT_WORK *work)
{
	work->data = work->buf;
	if(work->rol_cnt > 0)
	{
		work->data.rol.all = (work->buf.trg.all & getAddedBitData(work->rol_key).all);
		work->rol_cnt--;
	}
	else if(work->rol_cnt == 0)
	{
		work->data.rol.all = 0;
		work->data.rol.all = getAddedBitData(work->rol_key).all;
	}

	work->buf.trg.all = 0;
}

/**
 *
 */
void initRemoWork(REMO_WORK *work)
{
	work->wiimotes = wiiuse_init(1);
	work->maintask = 0;
	work->subtask = 0;
	{
		int i;
		for(i = 0; i < RAD_LOG_MAX; i++)
		{
			work->log_rad[i] = 0;
		}
	}
}

/**
 * work init
 */
void initWork(WORK *work)
{
	work->fps_anim = 0;
	work->rad = 0;
	work->screen = NULL;
	work->img_off = NULL;
	initInputWork(&(work->input));
	initRemoWork(&(work->remo));
}

/**
 * update
 */
int update(INPUT_DATA input, WORK *work)
{
	if(input.trg.key.escape)
	{
		return 0;
	}
	else
	{
		if(input.trg.key.F11)
		{
			work->screen = SDL_SetVideoMode(work->screen->w, work->screen->h, work->screen->format->BitsPerPixel, work->screen->flags ^ SDL_FULLSCREEN);
		}
		else
		{
			//deg
			{
				KEY_DATA *key = &(input.rol);
				float v = 0.2f;
				if((key->key.up)
				|| (key->key.left))
				{
					work->rad += v;
				}
				else if((key->key.down)
				|| (key->key.right))
				{
					work->rad -= v;
				}
			}
			//anim
			{
				work->fps_anim++;
				work->fps_anim %= 60;
			}

			//remo
			{
				REMO_WORK *remo = &(work->remo);
				wiimote **wiimotes  = remo->wiimotes;
				if(remo->maintask == 0)
				{
					int found = wiiuse_find(wiimotes, 1, 1);
					if(found > 0)
					{
						int connected = wiiuse_connect(wiimotes, 1);
						if(connected)
						{
							remo->maintask++;
							remo->subtask = 60;
							wiiuse_motion_sensing(wiimotes[0], 1);
							wiiuse_set_accel_threshold(wiimotes[0], 0);
							wiiuse_set_leds(wiimotes[0], WIIMOTE_LED_1 | WIIMOTE_LED_2 | WIIMOTE_LED_3 | WIIMOTE_LED_4);
							wiiuse_rumble(wiimotes[0], 1);
						}
					}
				}
				else
				{
					if(wiiuse_poll(wiimotes, 1))
					{
						if(wiimotes[0]->event == WIIUSE_DISCONNECT)
						{
							wiiuse_cleanup(wiimotes, 1);
							initRemoWork(remo);
						}
					}

					if(remo->maintask == 1)
					{
						if(remo->subtask > 0)
						{
							remo->subtask--;
						}
						else
						{
							wiiuse_rumble(wiimotes[0], 0);
							wiiuse_set_leds(wiimotes[0], WIIMOTE_LED_1);
							remo->maintask++;
							remo->subtask = 0;
						}
					}
					else if(remo->maintask == 2)
					{
						vec3b_t accel = wiimotes[0]->accel;
						{
							int i;
							for(i = 0; i < RAD_LOG_MAX - 1; i++)
							{
								int j = RAD_LOG_MAX - 1 - 1 - i;
								remo->log_rad[j + 1] = remo->log_rad[j];
							}
						}
						remo->log_rad[0] = atan2(accel.x - 128, accel.y - 128) + DEG2RAD(90);
					}
				}
			}
		}
		return 1;
	}
}

/**
 * fill
 */
void fillRect(SDL_Surface *suf, int x, int y, int w, int h, Uint32 color)
{
	SDL_Rect rect;
	rect.x = x;
	rect.y = y;
	rect.w = w;
	rect.h = h;
	SDL_FillRect(suf, &rect, color);
}

void drawBoxNum(SDL_Surface *suf, const char str[], int x, int y, int size, Uint32 color)
{
	static const char* list = ".+-0123456789";
	static const int data[13][5][3]=
	{
		//.
		{
			{0, 0, 0},
			{0, 0, 0},
			{0, 0, 0},
			{0, 0, 0},
			{0, 1, 0},
		},
		//+
		{
			{0, 0, 0},
			{0, 1, 0},
			{1, 1, 1},
			{0, 1, 0},
			{0, 0, 0},
		},
		//-
		{
			{0, 0, 0},
			{0, 0, 0},
			{1, 1, 1},
			{0, 0, 0},
			{0, 0, 0},
		},
		//0
		{
			{1, 1, 1},
			{1, 0, 1},
			{1, 0, 1},
			{1, 0, 1},
			{1, 1, 1},
		},
		//1
		{
			{0, 1, 0},
			{0, 1, 0},
			{0, 1, 0},
			{0, 1, 0},
			{0, 1, 0}
		},
		//2
		{

			{1, 1, 1},
			{0, 0, 1},
			{1, 1, 1},
			{1, 0, 0},
			{1, 1, 1}
		},
		//3
		{
			{1, 1, 1},
			{0, 0, 1},
			{1, 1, 1},
			{0, 0, 1},
			{1, 1, 1}
		},
		//4
		{
			{1, 0, 1},
			{1, 0, 1},
			{1, 1, 1},
			{0, 0, 1},
			{0, 0, 1}
		},
		//5
		{
			{1, 1, 1},
			{1, 0, 0},
			{1, 1, 1},
			{0, 0, 1},
			{1, 1, 1}
		},
		//6
		{
			{1, 1, 1},
			{1, 0, 0},
			{1, 1, 1},
			{1, 0, 1},
			{1, 1, 1}
		},
		//7
		{
			{1, 1, 1},
			{1, 0, 1},
			{0, 0, 1},
			{0, 1, 0},
			{0, 1, 0}
		},
		//8
		{
			{1, 1, 1},
			{1, 0, 1},
			{1, 1, 1},
			{1, 0, 1},
			{1, 1, 1}
		},
		//9
		{
			{1, 1, 1},
			{1, 0, 1},
			{1, 1, 1},
			{0, 0, 1},
			{1, 1, 1}
		}
	};

	//
	{
		//draw
		{
			size_t i;
			size_t cnt = strlen(str);
			for(i = 0; i < cnt; i++)
			{
				size_t index;
				for(index = 0; index < strlen(list); index++)
				{
					if(str[i] == list[index])
					{
						int p, q;
						for(q = 0; q < 5; q++)
						{
							for(p = 0; p < 3; p++)
							{
								if(data[index][q][p])
								{
									fillRect(suf, x + (i * (3 + 1) * size) + (p * size), y + (q * size), size, size, color);
								}
							}
						}
						break;
					}
				}
			}
		}
	}
}

/**
 * draw
 */
void draw(SDL_Surface *suf, WORK *work)
{
	fillRect(suf, 0, 0, suf->w, suf->h, 0xFF666666);

	//wii remocon
	{
		const int base_r = 285;
		const int base_x = (suf->w - (base_r * 2)) / 2;
		const int base_y = 10;
		const int center_x = base_x + base_r;
		const int center_y = base_y + base_r;
		const int col_r = 255;
		const int col_g = 192;
		const int col_b = 128;
		//const float rad = work->rad;
		float rad;

		//collect
		{
			double r = 0;
			int i;
			for(i = 0; i < RAD_LOG_MAX; i++)
			{
				r += work->remo.log_rad[i];
			}
			r /= i;
			rad = -(float)r;
		}

		//edge
		{
			SDL_DrawCircle(suf, base_x, base_y, base_x + (base_r * 2), base_y + (base_r * 2), col_r, col_g, col_b);
		}
		//box
		{
			int i;
			int pos[4][2];
			pos[0][0] = (int)(cos(rad + DEG2RAD(15)) * base_r);
			pos[0][1] = (int)(sin(rad + DEG2RAD(15)) * base_r);
			pos[1][0] = (int)(cos(rad + DEG2RAD(165)) * base_r);
			pos[1][1] = (int)(sin(rad + DEG2RAD(165)) * base_r);
			pos[2][0] = (int)(cos(rad + DEG2RAD(195)) * base_r);
			pos[2][1] = (int)(sin(rad + DEG2RAD(195)) * base_r);
			pos[3][0] = (int)(cos(rad + DEG2RAD(345)) * base_r);
			pos[3][1] = (int)(sin(rad + DEG2RAD(345)) * base_r);
			
			for(i = 0; i < 4; i++)
			{
				const int *src = pos[i];
				const int *tgt = pos[(i + 1) % 4];
				SDL_DrawLine(suf, center_x + src[0], center_y + src[1], center_x + tgt[0], center_y + tgt[1], col_r, col_g, col_b);
			}
		}
		//button
		{
			int i;
			int pos[6][3];

			//[1][2]
			{
				pos[0][0] = (int)(cos(rad) * base_r * 0.50);
				pos[0][1] = (int)(sin(rad) * base_r * 0.50);
				pos[0][2] = 15;
				pos[1][0] = (int)(cos(rad) * base_r * 0.75);
				pos[1][1] = (int)(sin(rad) * base_r * 0.75);
				pos[1][2] = 15;
			}
			//[+][home][-]
			{
				const double cx = cos(rad + DEG2RAD(0)) * base_r * 0.15;
				const double cy = sin(rad + DEG2RAD(0)) * base_r * 0.15;
				pos[2][0] = (int)((cx) + (cos(rad + DEG2RAD(0)) * base_r * 0.0));
				pos[2][1] = (int)((cy) + (sin(rad + DEG2RAD(0)) * base_r * 0.0));
				pos[2][2] = 10;
				pos[3][0] = (int)((cx) + (cos(rad + DEG2RAD(90)) * base_r * 0.15));
				pos[3][1] = (int)((cy) + (sin(rad + DEG2RAD(90)) * base_r * 0.15));
				pos[3][2] = 10;
				pos[4][0] = (int)((cx) + (cos(rad + DEG2RAD(270)) * base_r * 0.15));
				pos[4][1] = (int)((cy) + (sin(rad + DEG2RAD(270)) * base_r * 0.15));
				pos[4][2] = 10;
			}
			//[A]
			{
				pos[5][0] = (int)(cos(rad + DEG2RAD(180)) * base_r * 0.20);
				pos[5][1] = (int)(sin(rad + DEG2RAD(180)) * base_r * 0.20);
				pos[5][2] = 25;
			}

			for(i = 0; i < 6; i++)
			{
				const int *src = pos[i];
				const int r = src[2] * base_r / 285;
				SDL_DrawCircle(suf, center_x + src[0] - r, center_y + src[1] - r, center_x + src[0] + r, center_y + src[1] + r, col_r, col_g, col_b);
			}
		}
		//plus
		{
			const int cx = center_x + (int)(cos(rad + DEG2RAD(180)) * base_r * 0.65);
			const int cy = center_y + (int)(sin(rad + DEG2RAD(180)) * base_r * 0.65);
			int j;
			float deg = 0;
			for(j = 0; j < 2; j++)
			{
				int i;
				int pos[4][2];
				pos[0][0] = (int)(cos(rad + DEG2RAD(deg + 15)) * base_r * 0.2);
				pos[0][1] = (int)(sin(rad + DEG2RAD(deg + 15)) * base_r * 0.2);
				pos[1][0] = (int)(cos(rad + DEG2RAD(deg + 165)) * base_r * 0.2);
				pos[1][1] = (int)(sin(rad + DEG2RAD(deg + 165)) * base_r * 0.2);
				pos[2][0] = (int)(cos(rad + DEG2RAD(deg + 195)) * base_r * 0.2);
				pos[2][1] = (int)(sin(rad + DEG2RAD(deg + 195)) * base_r * 0.2);
				pos[3][0] = (int)(cos(rad + DEG2RAD(deg + 345)) * base_r * 0.2);
				pos[3][1] = (int)(sin(rad + DEG2RAD(deg + 345)) * base_r * 0.2);

				for(i = 0; i < 4; i++)
				{
					const int *src = pos[i];
					const int *tgt = pos[(i + 1) % 4];
					SDL_DrawLine(suf, cx + src[0], cy + src[1], cx + tgt[0], cy + tgt[1], col_r, col_g, col_b);
				}

				deg += 90;
			}
		}
		//deg
		{
			char buf[32];
			sprintf_s(buf,  sizeof(buf), "%f", RAD2DEG(rad));
			drawBoxNum(suf, buf, 780 - (strlen(buf) * ((3 + 1) * 3)), 550, 3, 0xFFFFFFFF);
		}
		//accel
		{
			double d[2];
			vec3b_t accel = work->remo.wiimotes[0]->accel;
			char buf[32];
			d[0] = RAD2DEG(atan2(accel.x - 128, accel.y - 128));
			d[1] = d[0] + 90;
			sprintf_s(buf,  sizeof(buf), "%d", accel.x - 128);
			drawBoxNum(suf, buf, 720, 470, 2, 0xFFFFFFFF);
			sprintf_s(buf,  sizeof(buf), "%d", accel.y - 128);
			drawBoxNum(suf, buf, 720, 490, 2, 0xFFFFFFFF);
			sprintf_s(buf,  sizeof(buf), "%d", accel.z - 128);
			drawBoxNum(suf, buf, 720, 510, 2, 0xFFFFFFFF);
			sprintf_s(buf,  sizeof(buf), "%f, f", d[0], d[1]);
			drawBoxNum(suf, buf, 720, 530, 2, 0xFFFFFFFF);
		}
		//seq
		{
			char buf[32];
			sprintf_s(buf,  sizeof(buf), "%d", work->remo.maintask);
			drawBoxNum(suf, buf, 700, 450, 2, 0xFFFFFFFF);
			sprintf_s(buf,  sizeof(buf), "%d", work->remo.subtask);
			drawBoxNum(suf, buf, 750, 450, 2, 0xFFFFFFFF);
		}
	}

	//fps anim
	{
		int i;
		int cnt = work->fps_anim;
		for(i = 0; i < 60; i++)
		{
			int j = (i + cnt - 30 + 60) % 60;
			Uint32 color = 0;
			int dist = 30 - i;
			int  r, g, b;
			if(dist < 0)
			{
				dist = -dist;
			}
			r = 0;
			g = dist * 255 / 30;
			b = 0;
			color |= ((r & 0xFF) << 16);
			color |= ((g & 0xFF) << 8);
			color |= ((b & 0xFF) << 0);
			fillRect(suf, (int)(10 + (13 * j)), 588, 10, 10, (0xFF000000 | color));
		}
	}
}

/**
 * entry point
 */
WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	WORK work;

	initWork(&work);

	if(SDL_Init(SDL_INIT_VIDEO) >= 0) 
	{
		//init
		{
			work.screen = SDL_SetVideoMode(800, 600, 32, SDL_SWSURFACE);
			work.img_off = SDL_CreateRGBSurface(SDL_SWSURFACE, 800, 600, 32, RMASK, GMASK, BMASK, AMASK);
		}

		//loop
		{
			const double WAIT = 1000.0 / 60;
			Uint32 last = SDL_GetTicks();
			int active = 1;
			while(active)
			{
				SDL_Event ev;
				int flg = 1;
				while(SDL_PollEvent(&ev))
				{
					if((ev.type == SDL_QUIT)
					|| (ev.type == SDL_KEYUP && ev.key.keysym.sym == SDLK_ESCAPE))
					{
						active = 0;
						break;
					}
					else if(ev.type == SDL_KEYUP)
					{
						releaseKey(&(work.input), ev.key.keysym.sym);
					}
					else if(ev.type == SDL_KEYDOWN)
					{
						pressKey(&(work.input), ev.key.keysym.sym);
					}
				}

				if(active)
				{
					Uint32 time = SDL_GetTicks();
					if((time - last) >= WAIT)
					{
						updateInput(&(work.input));
						if(update(work.input.data, &work))
						{
							//draw
							{
								draw(work.img_off, &work);
								SDL_BlitSurface(work.img_off, NULL, work.screen, NULL);
								SDL_UpdateRect(work.screen, 0, 0, 0, 0);
							}
						}
						else
						{
							active = 0;
							break;
						}

						last = time;
					}
					else
					{
						SDL_Delay(1);
					}
				}
				else
				{
					break;
				}
			}
		}

		//quit
		{
			wiiuse_disconnected(work.remo.wiimotes[0]);
			wiiuse_cleanup(work.remo.wiimotes, 1);
		}
		SDL_Quit();

	}
	return 0;
}